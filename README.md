# Docker-Compose Infrastructure Template

A stack of open source software designed as a base for docker compose projects.  The stack has monitoring, visualization and log collection prepared.

## Stack Components

CA Advisor - docker container data collection

Grafana - visualization software that can be used for both stack monitoring and quick observability front end

Graphite - Time series database

Loki - Log exporter

NGINX - Reverse proxy and TLS implementation

Node Exporter - Metric exporter

Postgresql - SQL Database

Promethius - Metric aggregator

Promtail - Log aggregator



## Setup

Install the Loki pluggin into Docker

    docker plugin install grafana/loki-docker-driver:2.9.1 --alias loki --grant-all-permissions

Update the username / password in the .env

Update the server name from localhost under nginx/default.conf

Run docker compose up -d

Log into postgres container and setup databases (optional)
    docker exec -it postgres /bin/bash
        example: psql -U username -c 'CREATE DATABASE "dbname"'

Log into Grafana at https://server.ip.address/grafana and reset the default password (admin/admin)
    Update the postgres datasource username, password and database name